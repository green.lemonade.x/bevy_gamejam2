#!/bin/sh

target="$1"

if [ -z "$target" ]; then
    echo "USAGE: build_release.sh TARGET"
    exit 1
fi

case "$target" in
linux)
    mkdir -p builds/linux
    cargo build --release --features bevy/wayland
    cp target/release/bevy_gamejam2 builds/linux/com_bine
    ouch c builds/linux/* assets builds/linux/COM-bine_linux.tar.gz
    ;;
wasm)
    mkdir -p builds/wasm
    cargo build --profile wasm-release --target wasm32-unknown-unknown
    wasm-opt -Oz --output optimized.wasm target/wasm32-unknown-unknown/wasm-release/bevy_gamejam2.wasm
    mv optimized.wasm target/wasm32-unknown-unknown/wasm-release/bevy_gamejam2.wasm
    wasm-bindgen --no-typescript --out-name com-bine --out-dir builds/wasm --target web target/wasm32-unknown-unknown/wasm-release/bevy_gamejam2.wasm
    rm COM-bine.zip
    ouch c builds/wasm/* assets builds/wasm/COM-bine_wasm.zip
    ;;
windows)
    mkdir -p builds/windows
    cargo build --release --target x86_64-pc-windows-gnu
    cp target/x86_64-pc-windows-gnu/release/bevy_gamejam2.exe builds/windows/com_bine.exe
    ouch c builds/windows/* assets builds/windows/COM-bine_windows.zip
    ;;
*)
    echo "Unknown target '$target'"
    echo 'Available targets are: `linux`, `wasm`, `windows'
esac
