# COM-bine
An extremely wealthy farmer bought a brand-new combine harvester capable of autonomously harvesting an entire field.
This combine harvester uses geostationary satellites to compute its position on the field full of grain.
Somehow, however, the satellites tend to rotate away from the Earth, causing the combine to drive the wrong route.
Now it is up to you to save the surrounding nature and the farmer's earnings by aligning the satellites and thus keeping the combine on track.
The Indicator located at the planet shows you how well aligned the satellite is.
Green = Well, Red = Not so well.

## Controls
* `W`,`S` - Pitch: Rotate the satellite around the axis along the solar panels.
* `Q`,`E` - Roll: Rotate the satellite around the axis along the thruster and the communication antenna.
* `A`,`D` - Yaw: Rotate the satellite around the remaining axis.
* `1`,`2`,`3`,`4` - Cycle through the satellites.
* `Mouse1` (`Left Click`) + `Moving the mouse` - Rotate your view around the satellite.

## Credits
- This work is based on "Low Poly Combine Harvestors" (https://sketchfab.com/3d-models/low-poly-combine-harvestors-84b8a341aabc421b8f7268c3a4b173f1) by iedalton (https://sketchfab.com/iedalton) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
- The specular Earth texture is from "Solar System Scope" (https://www.solarsystemscope.com/textures/) licensed under CC-BY-4.0 (https://creativecommons.org/licenses/by/4.0/)
- The albedo and clouds Earth textures are from NASA's "Visible Earth" project (https://visibleearth.nasa.gov/images/73967/february-blue-marble-next-generation and https://visibleearth.nasa.gov/images/57747/blue-marble-clouds, respectively)