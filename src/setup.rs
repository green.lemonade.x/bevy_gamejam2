use std::f32::consts::{FRAC_PI_2, FRAC_PI_6, PI, TAU};

use bevy::core_pipeline::clear_color::ClearColorConfig;
use bevy::prelude::shape::{Plane, Quad, UVSphere};
use bevy::prelude::*;
use bevy::render::camera::Projection;
use bevy::render::render_resource::Face;
use bevy::render::view::RenderLayers;
use bevy_dolly::prelude::*;
use bevy_rapier3d::prelude::*;
use bevy_turborand::GlobalRng;

use crate::camera_controller::{
    BackgroundCamera, CameraTarget, HudCamera, MainCamera, PlanetCamera,
};
use crate::combine::{Combine, Waypoint, Waypoints};
use crate::random_vec_xz;
use crate::satellite::{ActiveSatellite, Satellite, SatelliteBundle};
use crate::target::{Target, TargetIndicator};
use crate::tiling::Tile;

const PLANET_RADIUS: f32 = 1000.0;
const SATELLITE_COUNT: usize = 4;
const DRIFT_INTENSITY: f32 = 0.05;

const SATELLITE_LAYER: RenderLayers = RenderLayers::layer(0);
const PLANET_LAYER: RenderLayers = RenderLayers::layer(1);
const BACKGROUND_LAYER: RenderLayers = RenderLayers::layer(2);
const HUD_LAYER: RenderLayers = RenderLayers::layer(3);
const COMBINE_LAYER: RenderLayers = RenderLayers::layer(4);

pub struct SetupPlugin;

impl Plugin for SetupPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(satellite_setup)
            .add_startup_system(camera_setup)
            .add_startup_system(light_setup)
            .add_startup_system(planet_setup)
            .add_startup_system(background_setup)
            .add_startup_system(hud_setup)
            .add_startup_system(music_setup)
            .add_startup_system(grain_setup)
            .add_startup_system(combine_setup);
    }
}

fn satellite_setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    global_rng: ResMut<GlobalRng>,
) {
    let satellite = asset_server.load("models/satellite.gltf#Scene0");
    let rng = global_rng.into_inner();

    for i in 1..SATELLITE_COUNT {
        let pos = Vec3::new(
            (TAU * i as f32 / SATELLITE_COUNT as f32).cos() * 5.6 * PLANET_RADIUS,
            0.0,
            (TAU * i as f32 / SATELLITE_COUNT as f32).sin() * 5.6 * PLANET_RADIUS,
        );
        commands
            .spawn_bundle(SceneBundle {
                scene: satellite.clone(),
                transform: Transform::from_translation(pos)
                    .with_rotation(Quat::from_rotation_arc(Vec3::Y, -pos.normalize())),
                ..default()
            })
            .insert_bundle(SatelliteBundle {
                velocity: Velocity::angular(random_vec_xz(rng) * DRIFT_INTENSITY),
                ..default()
            })
            .insert(Satellite(i))
            .insert(Target(Vec3::ZERO))
            .insert(SATELLITE_LAYER);
    }

    commands
        .spawn_bundle(SceneBundle {
            scene: satellite,
            transform: Transform::from_xyz(5.6 * PLANET_RADIUS, 0.0, 0.0)
                .with_rotation(Quat::from_rotation_arc(Vec3::Y, Vec3::NEG_X)),
            ..default()
        })
        .insert_bundle(SatelliteBundle {
            velocity: Velocity::angular(random_vec_xz(rng) * DRIFT_INTENSITY),
            ..default()
        })
        .insert(Satellite(0))
        .insert(Target(Vec3::ZERO))
        .insert(ActiveSatellite)
        .insert(CameraTarget)
        .insert(SATELLITE_LAYER);
}

fn camera_setup(mut commands: Commands) {
    commands
        .spawn()
        .insert(
            Rig::builder()
                .with(Position::new(Vec3::ZERO))
                .with(Smooth::new_position(1.0))
                .with(Rotation::new(Quat::default()))
                .with(Smooth::new_rotation(1.5))
                .with(Arm::new(Vec3::Z * 24.0))
                .build(),
        )
        .insert(MainCamera)
        .insert_bundle(Camera3dBundle {
            transform: Transform::from_translation(Vec3::new(8.0, 10.0, 25.0))
                .looking_at(Vec3::ZERO, Vec3::Y),
            camera_3d: Camera3d {
                clear_color: ClearColorConfig::None,
                ..default()
            },
            camera: Camera {
                priority: 3,
                ..default()
            },
            ..default()
        })
        .insert(MainCamera)
        .insert(SATELLITE_LAYER)
        .insert(Tile {
            x: 0.4..1.0,
            y: 0.0..1.0,
        });
}

fn light_setup(mut commands: Commands) {
    commands.spawn_bundle(DirectionalLightBundle {
        transform: Transform::from_rotation(Quat::from_xyzw(
            0.32001078,
            -0.1157244,
            -0.068004206,
            -0.9378576,
        )),
        ..default()
    });
}

fn planet_setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(
                UVSphere {
                    radius: PLANET_RADIUS,
                    sectors: 256,
                    stacks: 128,
                }
                .into(),
            ),
            material: materials.add(StandardMaterial {
                base_color_texture: Some(asset_server.load("images/earth_albedo.jpg")),
                metallic_roughness_texture: Some(asset_server.load("images/earth_specular.jpg")),
                ..default()
            }),
            transform: Transform::from_rotation(Quat::from_rotation_x(-FRAC_PI_2)),
            ..default()
        })
        .insert(PLANET_LAYER);
    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(
                UVSphere {
                    radius: PLANET_RADIUS + 10.0,
                    sectors: 256,
                    stacks: 128,
                }
                .into(),
            ),
            material: materials.add(StandardMaterial {
                base_color_texture: Some(asset_server.load("images/earth_clouds.png")),
                alpha_mode: AlphaMode::Blend,
                ..default()
            }),
            transform: Transform::from_rotation(Quat::from_rotation_x(-FRAC_PI_2)),
            ..default()
        })
        .insert(PLANET_LAYER);

    commands
        .spawn_bundle(Camera3dBundle {
            camera_3d: Camera3d {
                clear_color: ClearColorConfig::None,
                ..default()
            },
            camera: Camera {
                priority: 1,
                ..default()
            },
            ..default()
        })
        .insert(PlanetCamera)
        .insert(PLANET_LAYER)
        .insert(Tile {
            x: 0.4..1.0,
            y: 0.0..1.0,
        });
}

fn background_setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(
                UVSphere {
                    radius: 10.0,
                    sectors: 256,
                    stacks: 128,
                }
                .into(),
            ),
            material: materials.add(StandardMaterial {
                base_color: Color::BLACK,
                cull_mode: Some(Face::Front),
                unlit: true,
                ..default()
            }),
            ..default()
        })
        .insert(BACKGROUND_LAYER);

    commands
        .spawn_bundle(Camera3dBundle::default())
        .insert(BackgroundCamera)
        .insert(BACKGROUND_LAYER)
        .insert(Tile {
            x: 0.4..1.0,
            y: 0.0..1.0,
        });
}

fn hud_setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(Quad::new(Vec2::new(0.15, 0.15)).into()),
            material: materials.add(StandardMaterial {
                base_color_texture: Some(asset_server.load("images/target.png")),
                unlit: true,
                alpha_mode: AlphaMode::Blend,
                ..default()
            }),
            transform: Transform::from_xyz(0.0, 0.0, -1.0),
            ..default()
        })
        .insert(HUD_LAYER)
        .insert(TargetIndicator);

    commands
        .spawn_bundle(Camera3dBundle {
            camera_3d: Camera3d {
                clear_color: ClearColorConfig::None,
                ..default()
            },
            camera: Camera {
                priority: 2,
                ..default()
            },
            ..default()
        })
        .insert(HudCamera)
        .insert(Tile {
            x: 0.4..1.0,
            y: 0.0..1.0,
        })
        .insert(HUD_LAYER);
}

fn music_setup(asset_server: Res<AssetServer>, audio: Res<Audio>) {
    let music = asset_server.load("audio/music.ogg");
    audio.play_with_settings(music, PlaybackSettings::LOOP.with_volume(0.5));
}

fn grain_setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(Plane { size: 500.0 }.into()),
            material: materials.add(StandardMaterial {
                base_color: Color::rgb(0.34, 0.19, 0.12),
                reflectance: 0.0,
                metallic: 0.0,
                ..default()
            }),
            transform: Transform::from_xyz(0.0, -0.4, 0.0),
            ..default()
        })
        .insert(COMBINE_LAYER);
    // x in [-10,10]; z in [-20,20]
    for x in -6..=5 {
        for z in -10..=10 {
            commands
                .spawn_bundle(PbrBundle {
                    mesh: asset_server.load("models/grain.gltf#Mesh0/Primitive0"),
                    material: asset_server.load("models/grain.gltf#Material0"),
                    transform: Transform::from_xyz(x as f32 * 2.0, 0.1, z as f32 * 2.0),
                    ..default()
                })
                .insert(Collider::cuboid(1.0, 0.5, 1.0))
                .insert(Sensor)
                .insert(ActiveCollisionTypes::STATIC_STATIC)
                .insert(COMBINE_LAYER);
        }
    }
}

fn combine_setup(
    mut commands: Commands,
    global_rng: ResMut<GlobalRng>,
    asset_server: Res<AssetServer>,
) {
    let rng = global_rng.into_inner();

    let waypoints = {
        vec![
            Waypoint(Vec3::new(9.0, 0.0, -20.0)),
            // Waypoint(Vec3::new(9.0, 0.0, -10.0)),
            Waypoint(Vec3::new(9.0, 0.0, 0.0)),
            // Waypoint(Vec3::new(9.0, 0.0, 10.0)),
            Waypoint(Vec3::new(9.0, 0.0, 20.0)),
            Waypoint(Vec3::new(5.0, 0.0, 20.0)),
            // Waypoint(Vec3::new(5.0, 0.0, 10.0)),
            Waypoint(Vec3::new(5.0, 0.0, 0.0)),
            // Waypoint(Vec3::new(5.0, 0.0, -10.0)),
            Waypoint(Vec3::new(5.0, 0.0, -20.0)),
            Waypoint(Vec3::new(1.0, 0.0, -20.0)),
            // Waypoint(Vec3::new(1.0, 0.0, -10.0)),
            Waypoint(Vec3::new(1.0, 0.0, 0.0)),
            // Waypoint(Vec3::new(1.0, 0.0, 10.0)),
            Waypoint(Vec3::new(1.0, 0.0, 20.0)),
            Waypoint(Vec3::new(-3.0, 0.0, 20.0)),
            // Waypoint(Vec3::new(-3.0, 0.0, 10.0)),
            Waypoint(Vec3::new(-3.0, 0.0, 0.0)),
            // Waypoint(Vec3::new(-3.0, 0.0, -10.0)),
            Waypoint(Vec3::new(-3.0, 0.0, -20.0)),
            Waypoint(Vec3::new(-7.0, 0.0, -20.0)),
            // Waypoint(Vec3::new(-7.0, 0.0, -10.0)),
            Waypoint(Vec3::new(-7.0, 0.0, 0.0)),
            // Waypoint(Vec3::new(-7.0, 0.0, 10.0)),
            Waypoint(Vec3::new(-7.0, 0.0, 20.0)),
            Waypoint(Vec3::new(-11.0, 0.0, 20.0)),
            // Waypoint(Vec3::new(-11.0, 0.0, 10.0)),
            Waypoint(Vec3::new(-11.0, 0.0, 0.0)),
            // Waypoint(Vec3::new(-11.0, 0.0, -10.0)),
            Waypoint(Vec3::new(-11.0, 0.0, -20.0)),
        ]
    };

    let mut errors = vec![Vec3::ZERO];

    for _ in 1..waypoints.len() - 1 {
        errors.push(random_vec_xz(rng) * 2.0);
    }
    errors.push(Vec3::ZERO);

    commands
        .spawn_bundle(PbrBundle {
            mesh: asset_server.load("models/combine.gltf#Mesh0/Primitive0"),
            material: asset_server.load("models/combine.gltf#Material0"),
            transform: Transform::from_xyz(9.0, 0.0, -23.0)
                .with_rotation(Quat::from_rotation_y(PI)),
            ..default()
        })
        .insert(Combine::default())
        .insert(Waypoints {
            waypoints,
            error: errors,
            active_index: 0,
            repeats: false,
        })
        .insert(Collider::cuboid(1.5, 0.5, 0.5))
        .insert(ActiveEvents::COLLISION_EVENTS)
        .insert(COMBINE_LAYER);

    commands.spawn_bundle(PointLightBundle {
        transform: Transform::from_xyz(-200.0, 100.0, -200.0),
        point_light: PointLight {
            intensity: 10000000.0,
            range: 1000.0,
            ..default()
        },
        ..default()
    });
    commands.spawn_bundle(PointLightBundle {
        transform: Transform::from_xyz(200.0, 100.0, 200.0),
        point_light: PointLight {
            intensity: 10000000.0,
            range: 1000.0,
            ..default()
        },
        ..default()
    });

    commands
        .spawn_bundle(Camera3dBundle {
            transform: Transform::from_translation(Vec3::Y * 50.0 + Vec3::NEG_Z * 60.0)
                .looking_at(Vec3::NEG_Z * 0.0, Vec3::Y),
            camera: Camera {
                priority: 4,
                ..default()
            },
            camera_3d: Camera3d {
                clear_color: ClearColorConfig::None,
                ..default()
            },
            projection: Projection::Perspective(PerspectiveProjection {
                fov: FRAC_PI_6,
                ..default()
            }),
            ..default()
        })
        .insert(Tile {
            x: 0.0..0.4,
            y: 0.0..1.0,
        })
        .insert(COMBINE_LAYER);
}
