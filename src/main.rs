#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use bevy::prelude::*;
use bevy_rapier3d::prelude::*;
use bevy_turborand::GlobalRng;
use bevy_turborand::{DelegatedRng, RngPlugin};

use crate::camera_controller::CameraControlPlugin;
use crate::combine::CombinePlugin;
use crate::grain::GrainPlugin;
use crate::satellite::SatellitePlugin;
use crate::setup::SetupPlugin;
use crate::target::TargetPlugin;
use crate::tiling::TilingPlugin;

mod camera_controller;
mod combine;
mod grain;
mod satellite;
mod setup;
mod target;
mod tiling;

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: String::from("COM-bine"),
            ..default()
        })
        .insert_resource(AmbientLight {
            brightness: 0.0,
            ..default()
        })
        .add_plugins(DefaultPlugins)
        .add_system(bevy::window::close_on_esc)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .add_plugin(RngPlugin::default())
        .add_plugin(SetupPlugin)
        .add_plugin(SatellitePlugin)
        .add_plugin(CameraControlPlugin)
        .add_plugin(TilingPlugin)
        .add_plugin(TargetPlugin)
        .add_plugin(CombinePlugin)
        .add_plugin(GrainPlugin)
        .run();
}

fn random_vec_xz(rng: &mut GlobalRng) -> Vec3 {
    Vec3::new(rng.f32_normalized(), 0.0, rng.f32_normalized())
}
