use crate::combine::Inaccuracy;
use bevy::prelude::*;

use crate::satellite::{ActiveSatellite, Satellite};

const TARGET_PRECISION: f32 = 0.95;

const RED: Vec4 = Vec4::new(1.0, 0.0, 0.0, 1.0);
const YELLOW: Vec4 = Vec4::new(0.89, 0.77, 0.14, 1.0);
const GREEN: Vec4 = Vec4::new(0.28, 0.82, 0.11, 1.0);

pub struct TargetPlugin;

impl Plugin for TargetPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(update_indicator_position)
            .add_system(update_indicator_color)
            .add_system(update_inaccuracy);
    }
}

#[derive(Component)]
pub struct TargetIndicator;

#[derive(Component)]
pub struct Target(pub Vec3);

fn update_indicator_position(
    target_query: Query<(&Target, &Transform), (Added<ActiveSatellite>, Without<TargetIndicator>)>,
    mut indicator_transform_query: Query<&mut Transform, With<TargetIndicator>>,
) {
    if let Ok((target, satellite_transform)) = target_query.get_single() {
        let mut indicator_transform = indicator_transform_query.single_mut();
        let mut transform = Transform::from_xyz(0.0, 0.0, -1.0);

        let rotation = Transform::from_translation(satellite_transform.translation)
            .looking_at(target.0, Vec3::Y)
            .rotation;
        transform.rotate_around(Vec3::ZERO, rotation);

        *indicator_transform = transform;
    }
}

// TODO: Use inaccuracy
fn update_indicator_color(
    satellite_transform_query: Query<&Transform, With<ActiveSatellite>>,
    indicator_query: Query<(&Transform, &Handle<StandardMaterial>), With<TargetIndicator>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let satellite_transform = satellite_transform_query.single();
    let (indicator_transform, material_handle) = indicator_query.single();

    let satellite_direction = (satellite_transform.rotation * Vec3::Y).normalize();
    let indicator_direction = indicator_transform.translation.normalize();

    let precision = satellite_direction.dot(indicator_direction);

    let material = materials.get_mut(material_handle).unwrap();

    material.base_color = if precision < TARGET_PRECISION {
        RED.lerp(YELLOW, precision / TARGET_PRECISION).into()
    } else {
        YELLOW
            .lerp(
                GREEN,
                (precision - TARGET_PRECISION) / (1.0 - TARGET_PRECISION),
            )
            .into()
    };
}

fn update_inaccuracy(
    satellite_query: Query<(&Transform, &Target), With<Satellite>>,
    mut combine_inaccuracy: ResMut<Inaccuracy>,
) {
    let mut overall_precision = 0.0;
    for (satellite_transform, Target(target_position)) in satellite_query.iter() {
        let target_direction = (*target_position - satellite_transform.translation).normalize();
        let satellite_direction = satellite_transform.rotation * Vec3::Y;

        // Map dot product from [-1,1] to [0,2] so that -1 -> 2, 1 -> 0.
        let satellite_precision = -target_direction.dot(satellite_direction) + 1.0;
        overall_precision += satellite_precision;
    }
    combine_inaccuracy.0 = overall_precision;
}
