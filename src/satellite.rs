use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

use crate::camera_controller::CameraTarget;

pub struct SatellitePlugin;

impl Plugin for SatellitePlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<SatelliteSettings>()
            .insert_resource(RapierConfiguration {
                gravity: Vec3::ZERO,
                ..default()
            })
            .insert_resource(Status(false))
            .add_system(satellite_motion)
            .add_system(change_active_satellite);
    }
}

pub struct Status(bool);

#[derive(Component)]
pub struct Satellite(pub usize);

#[derive(Component)]
pub struct ActiveSatellite;

#[derive(Bundle)]
pub struct SatelliteBundle {
    pub rigid_body: RigidBody,
    pub external_force: ExternalForce,
    pub velocity: Velocity,
    pub mass_properties: AdditionalMassProperties,
    pub sleeping: Sleeping,
}

impl Default for SatelliteBundle {
    fn default() -> Self {
        Self {
            rigid_body: RigidBody::Dynamic,
            external_force: ExternalForce::default(),
            velocity: Velocity::default(),
            mass_properties: AdditionalMassProperties::MassProperties(MassProperties {
                mass: 1.0,
                principal_inertia: Vec3::splat(1.0),
                ..default()
            }),
            sleeping: Sleeping::disabled(),
        }
    }
}

pub struct SatelliteSettings {
    pub thrust_sensitivity: f32,
    pub rotation_sensitivity: f32,
    pub min_angular_velocity: f32,
}

impl Default for SatelliteSettings {
    fn default() -> Self {
        Self {
            thrust_sensitivity: 0.02,
            rotation_sensitivity: 0.5,
            min_angular_velocity: 0.001,
        }
    }
}

fn satellite_motion(
    mut satellite_query: Query<
        (&mut ExternalForce, &Transform, Option<&ActiveSatellite>),
        With<Satellite>,
    >,
    keyboard_input: Res<Input<KeyCode>>,
    satellite_settings: Res<SatelliteSettings>,
) {
    for (mut external_force, transform, active) in satellite_query.iter_mut() {
        let mut torque = Vec3::default();

        let delta_rotation = satellite_settings.rotation_sensitivity;

        if active.is_some() {
            if keyboard_input.pressed(KeyCode::W) {
                torque.x = -delta_rotation;
            } else if keyboard_input.pressed(KeyCode::S) {
                torque.x = delta_rotation;
            }
        }

        if active.is_some() {
            if keyboard_input.pressed(KeyCode::D) {
                torque.z = -delta_rotation;
            } else if keyboard_input.pressed(KeyCode::A) {
                torque.z = delta_rotation;
            }
        }

        if active.is_some() {
            if keyboard_input.pressed(KeyCode::Q) {
                torque.y = -delta_rotation;
            } else if keyboard_input.pressed(KeyCode::E) {
                torque.y = delta_rotation;
            }
        }

        external_force.torque = transform.rotation * torque;
    }
}

fn change_active_satellite(
    mut commands: Commands,
    active_satellite_query: Query<Entity, With<ActiveSatellite>>,
    satellites_query: Query<(Entity, &Satellite)>,
    keyboard_input: Res<Input<KeyCode>>,
) {
    let selected_satellite = [
        KeyCode::Key1,
        KeyCode::Key2,
        KeyCode::Key3,
        KeyCode::Key4,
        KeyCode::Key5,
        KeyCode::Key6,
        KeyCode::Key7,
        KeyCode::Key8,
        KeyCode::Key9,
        KeyCode::Key0,
    ]
    .into_iter()
    .enumerate()
    .find_map(|(i, key)| keyboard_input.just_pressed(key).then_some(i));

    if let Some(selected_satellite) = selected_satellite {
        if let Some((entity, _)) = satellites_query
            .iter()
            .find(|(_, s)| s.0 == selected_satellite)
        {
            let active_satellite_entity = active_satellite_query.single();
            commands
                .entity(active_satellite_entity)
                .remove_bundle::<(ActiveSatellite, CameraTarget)>();
            commands
                .entity(entity)
                .insert_bundle((ActiveSatellite, CameraTarget));
        }
    }
}
