use crate::combine::Combine;
use bevy::prelude::*;
use bevy_rapier3d::prelude::*;

pub struct GrainPlugin;

pub struct GrainMowed(f32);

impl Plugin for GrainPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(GrainMowed(0.0))
            .add_system_to_stage(CoreStage::PostUpdate, print_events);
    }
}

fn print_events(
    mut commands: Commands,
    combine_query: Query<Entity, With<Combine>>,
    mut collision_events: EventReader<CollisionEvent>,
    time: Res<Time>,
    mut grain_mowed: ResMut<GrainMowed>,
) {
    for event in collision_events.iter() {
        if let CollisionEvent::Started(entity1, entity2, _) = event {
            let grain = if combine_query.single() == *entity1 {
                entity2
            } else {
                entity1
            };

            if time.seconds_since_startup() > 0.0001 {
                commands.entity(*grain).despawn();
                grain_mowed.0 += 1.0;
            }
        }
    }
}
