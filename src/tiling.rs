use bevy::prelude::*;
use bevy::render::camera::Viewport;
use std::cmp::max;
use std::ops::Range;

#[derive(Component, Clone)]
pub struct Tile {
    pub x: Range<f32>,
    pub y: Range<f32>,
}

pub struct WasResized(bool);

pub struct TilingPlugin;

impl Plugin for TilingPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(WasResized(false))
            .add_system_to_stage(CoreStage::Last, resize_window)
            .add_system(tiling_system);
    }
}

fn tiling_system(mut query: Query<(&mut Camera, &mut Tile)>, windows: Res<Windows>) {
    if windows.is_changed() {
        if let Some(window) = windows.get_primary() {
            if window.physical_height() > 0 && window.physical_width() > 0 {
                for (mut camera, tile) in query.iter_mut() {
                    let start_y = (window.physical_height() as f32 * tile.y.start) as u32 + 1;
                    let end_y = (window.physical_height() as f32 * tile.y.end) as u32 - 1;

                    let start_x = (window.physical_width() as f32 * tile.x.start) as u32 + 1;
                    let end_x =
                        max((window.physical_width() as f32 * tile.x.end) as u32 - 1, 0) as u32;

                    camera.viewport = Some(Viewport {
                        physical_position: UVec2::new(start_x, start_y),
                        physical_size: UVec2::new(end_x - start_x, end_y - start_y),
                        ..default()
                    });
                }
            }
        };
    }
}

fn resize_window(mut windows: ResMut<Windows>, mut was_resized: ResMut<WasResized>) {
    if !was_resized.0 {
        let window = windows.primary_mut();
        window.set_resolution(window.width() + 1.0, window.height() + 1.0);
        was_resized.0 = true;
    }
}
