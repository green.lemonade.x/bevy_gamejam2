use bevy::prelude::*;

pub struct CombinePlugin;

#[derive(PartialEq, Copy, Clone, Debug)]
pub struct Waypoint(pub Vec3);

#[derive(Component, Debug)]
pub struct Waypoints {
    pub waypoints: Vec<Waypoint>,
    pub error: Vec<Vec3>,
    pub active_index: usize,
    pub repeats: bool,
}

#[derive(Component)]
pub struct Combine {
    pub speed: f32,
    pub is_active: bool,
}

impl Default for Combine {
    fn default() -> Self {
        Self {
            speed: 1.0,
            is_active: true,
        }
    }
}

pub struct Inaccuracy(pub f32);

pub struct SmoothIterations(pub usize);

impl Plugin for CombinePlugin {
    fn build(&self, app: &mut App) {
        app.add_system(move_combine)
            // .add_system(smooth_waypoints)
            .insert_resource(Inaccuracy(0.0))
            .insert_resource(SmoothIterations(5));
    }
}

fn move_combine(
    time: Res<Time>,
    mut combine_query: Query<(&mut Transform, &mut Combine)>,
    mut waypoint_query: Query<&mut Waypoints>,
    inaccuracy: Res<Inaccuracy>,
) {
    let dt = time.delta_seconds();
    let inaccuracy = inaccuracy.0;

    let (mut combine_transform, mut combine) = combine_query.single_mut();
    let mut waypoints = waypoint_query.single_mut();

    let current_waypoint = waypoints.waypoints[waypoints.active_index].0
        + waypoints.error[waypoints.active_index] * inaccuracy;

    let to_direction = (current_waypoint - combine_transform.translation).normalize();
    if combine.is_active {
        combine_transform.translation += to_direction * dt * combine.speed;
    }

    let delta = (-combine_transform.translation + current_waypoint)
        .normalize()
        .dot(to_direction);

    combine_transform.rotation = if delta.signum() == 1.0 {
        combine_transform
            .looking_at(current_waypoint, Vec3::Y)
            .rotation
    } else {
        combine_transform.rotation
    };

    if delta.signum() == -1.0 && waypoints.active_index < waypoints.waypoints.len() - 1 {
        waypoints.active_index += 1;
    } else if delta < 0.02 && waypoints.active_index == waypoints.waypoints.len() - 1 {
        if waypoints.repeats {
            waypoints.active_index = 0;
        } else {
            combine.is_active = false;
        }
    }
}

fn _smooth_waypoints(
    mut waypoint_query: Query<&mut Waypoints, Added<Waypoints>>,
    iterations: Res<SmoothIterations>,
) {
    for mut waypoints in waypoint_query.iter_mut() {
        for _ in 0..iterations.0 {
            let mut new_waypoints = waypoints.waypoints.clone();
            let mut new_errors = waypoints.error.clone();
            for i in 1..waypoints.waypoints.len() - 1 {
                let (current_waypoint, current_error) =
                    (waypoints.waypoints[i].0, waypoints.error[i]);

                let (next_waypoint, next_error) = if waypoints.waypoints.len() > i + 1 {
                    (waypoints.waypoints[i + 1].0, waypoints.error[i + 1])
                } else {
                    (
                        waypoints.waypoints[waypoints.waypoints.len() - 1].0,
                        waypoints.error[waypoints.error.len() - 1],
                    )
                };

                let (last_waypoint, last_error) = if i > 0 {
                    (waypoints.waypoints[i - 1].0, waypoints.error[i - 1])
                } else {
                    (waypoints.waypoints[0].0, waypoints.error[0])
                };

                let _last_to_current = current_waypoint - last_waypoint;
                let _current_to_next = next_waypoint - current_waypoint;

                // if last_to_current.normalize().dot(current_to_next.normalize()) < threshold {
                // find center of circle through three points last = A,current = B,next = C
                // first find local coordinate system (u,v,w) with points on plane (u,v)
                // u = B - A
                let b_a = current_waypoint - last_waypoint;
                let u = b_a.normalize();
                // w = (C - A) x u
                let c_a = next_waypoint - last_waypoint;
                let w = c_a.cross(b_a).normalize();
                // v = w x u
                let v = w.cross(u);

                // get the 2D coordinates of the points
                let b = Vec2::new(b_a.dot(u), 0.0);
                let c = Vec2::new(c_a.dot(u), c_a.dot(v));

                // center lies on x = b_x / 2 --> coordinates (b_x/2,h)
                let b_x_half = b.x / 2.0;
                let h = ((c.x - b_x_half) * (c.x - b_x_half) + c.y * c.y - b_x_half * b_x_half)
                    / (2.0 * c.y);

                // Calculate center in 3d space
                let center = current_waypoint + b_x_half * u + h * v;

                // and the radius
                let radius = (current_waypoint - center).length();

                // find directions perpendicular to the sides of ABC by calculating the center of AB and BC.
                let mid_last_current = (last_waypoint + current_waypoint) / 2.0;
                let mid_current_next = (current_waypoint + next_waypoint) / 2.0;

                let dir_last_current = (mid_last_current - center).normalize();
                let dir_current_next = (mid_current_next - center).normalize();

                // calculate new points on circle
                let new_point_before = center + dir_last_current * radius;
                let _new_point_after = center + dir_current_next * radius;

                let new_point_fixed = 2.0 * mid_last_current - new_point_before;

                // new_waypoints.insert(i + 1, Waypoint(new_point_after));
                // new_errors.insert(i + 1, ((current_error + next_error) / 2.0).normalize());
                new_waypoints.insert(2 * i - 1, Waypoint(new_point_fixed));
                new_errors.insert(2 * i - 1, ((last_error + current_error) / 2.0).normalize());
                // }
            }
            waypoints.waypoints = new_waypoints;
            waypoints.error = new_errors;
            dbg!(&waypoints.waypoints);
            dbg!(&waypoints.waypoints);
        }
    }
}
