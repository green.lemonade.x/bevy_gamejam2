use bevy::input::mouse::MouseMotion;
use bevy::prelude::*;
use bevy_dolly::prelude::*;

#[derive(Component)]
pub struct MainCamera;

#[derive(Component)]
pub struct CameraTarget;

#[derive(Component)]
pub struct BackgroundCamera;

#[derive(Component)]
pub struct HudCamera;

#[derive(Component)]
pub struct PlanetCamera;

pub struct CameraControlPlugin;

pub struct CameraSettings {
    pub orbit_sensitivity: Vec3,
    pub zoom_sensitivity: f32,
    pub target_offset: f32,
}

impl Plugin for CameraControlPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(update_camera)
            .add_system(sync_cameras)
            .add_dolly_component(MainCamera)
            .insert_resource(CameraSettings {
                orbit_sensitivity: Vec3::new(1.0, 1.0, 1.0) / 90.0,
                zoom_sensitivity: 0.5,
                target_offset: 24.0,
            });
    }
}

fn update_camera(
    time: Res<Time>,
    mut mouse_events: EventReader<MouseMotion>,
    mouse_button_input: Res<Input<MouseButton>>,
    mut query: Query<&mut Rig, With<MainCamera>>,
    mut camera_settings: ResMut<CameraSettings>,
    target_query: Query<&Transform, With<CameraTarget>>,
) {
    let dt = time.delta_seconds();

    let mut rig = query.single_mut();
    let target_transform = target_query.single();
    let camera_driver = rig.driver_mut::<Position>();

    camera_driver.position = target_transform.translation;

    let mut mouse_delta = Vec2::ZERO;

    for mouse_event in mouse_events.iter() {
        mouse_delta += mouse_event.delta;
    }

    mouse_delta.x = mouse_delta.x.clamp(-360.0 * dt, 360.0 * dt);
    mouse_delta.y = mouse_delta.y.clamp(-360.0 * dt, 360.0 * dt);

    let axis_up = rig.final_transform.rotation * Vec3::NEG_Y;
    let axis_right = rig.final_transform.rotation * Vec3::NEG_X;
    let axis_front = rig.final_transform.rotation * Vec3::Z;

    let rotation_delta = if mouse_button_input.pressed(MouseButton::Left) {
        Quat::from_axis_angle(axis_up, mouse_delta.x * camera_settings.orbit_sensitivity.x)
            * Quat::from_axis_angle(
                axis_right,
                mouse_delta.y * camera_settings.orbit_sensitivity.y,
            )
    } else if mouse_button_input.pressed(MouseButton::Middle) {
        Quat::from_axis_angle(
            axis_front,
            mouse_delta.x * camera_settings.orbit_sensitivity.z,
        )
    } else {
        Quat::default()
    };

    let camera_driver = rig.driver_mut::<Rotation>();
    camera_driver.rotation = rotation_delta * camera_driver.rotation;

    let camera_driver = rig.driver_mut::<Arm>();

    let x = camera_settings.target_offset - camera_driver.offset.z;
    camera_driver.offset.z += x.signum() * x * x * 2.0 * dt;

    if x.abs() < 0.01 {
        camera_settings.target_offset = camera_driver.offset.z;
    }
}

fn sync_cameras(
    rig_query: Query<&Rig, With<MainCamera>>,
    mut camera_set: ParamSet<(
        Query<&mut Transform, With<BackgroundCamera>>,
        Query<&mut Transform, With<HudCamera>>,
        Query<&mut Transform, With<PlanetCamera>>,
    )>,
) {
    let rig_transform = rig_query.single().final_transform;

    camera_set.p0().single_mut().rotation = rig_transform.rotation;
    camera_set.p1().single_mut().rotation = rig_transform.rotation;
    *camera_set.p2().single_mut() =
        Transform::from_translation(rig_transform.position).with_rotation(rig_transform.rotation);
}
